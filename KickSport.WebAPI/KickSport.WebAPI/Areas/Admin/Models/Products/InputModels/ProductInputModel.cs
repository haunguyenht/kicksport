﻿using KickSport.Data.Common.Constant;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KickSport.WebAPI.Areas.Models.Products.InputModels
{
    public class ProductInputModel
    {
        [Required]
        [StringLength(Validation.NameMaximumLength, MinimumLength = Validation.NameMinimumLength, ErrorMessage = Validation.NameErrorMessage)]
        public string Name { get; set; }

        [Required]
        public string Category { get; set; }

        public List<string> Ingredients { get; set; }

        [Required]
        [StringLength(Validation.DescriptionMaximumLength, MinimumLength = Validation.DescriptionMinimumLength, ErrorMessage = Validation.DescriptionErrorMessage)]
        public string Description { get; set; }

        [Required]
        [MinLength(Validation.ImageMinimumLength, ErrorMessage = Validation.ImageErrorMessage)]
        [RegularExpression(Validation.ImageRegex, ErrorMessage = Validation.ImageRegexErrorMessage)]
        public string Image { get; set; }

        [Range(Validation.MinimumWeight, Validation.MaximumWeight, ErrorMessage = Validation.WeightErrorMessage)]
        public int Weight { get; set; }

        [Range(Validation.MinimumPrice, Double.MaxValue, ErrorMessage = Validation.PriceErrorMessage)]
        public decimal Price { get; set; }
    }
}
