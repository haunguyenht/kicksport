﻿using KickSport.Data.Common.Constant;
using System.ComponentModel.DataAnnotations;

namespace KickSport.WebAPI.Areas.Models.Ingredients.InputModels
{
    public class IngredientInputModel
    {
        [Required]
        [StringLength(Validation.NameMaximumLength, MinimumLength = Validation.NameMinimumLength, ErrorMessage = Validation.NameErrorMessage)]
        public string Name { get; set; }
    }
}
