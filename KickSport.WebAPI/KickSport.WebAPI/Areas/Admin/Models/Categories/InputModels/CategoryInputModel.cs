﻿using KickSport.Data.Common.Constant;
using System.ComponentModel.DataAnnotations;

namespace KickSport.WebAPI.Areas.Models.Categories.InputModels
{
    public class CategoryInputModel
    {
        [Required]
        [StringLength(Validation.NameMaximumLength, MinimumLength = Validation.NameMinimumLength, ErrorMessage = Validation.NameErrorMessage)]
        public string Name { get; set; }
    }
}
