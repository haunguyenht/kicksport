﻿using KickSport.WebAPI.Models.Products.ViewModels;
using System.Threading.Tasks;

namespace KickSport.WebAPI.Hubs.Contracts
{
    public interface IProductsHubClient
    {
        Task BroadcastProduct(ProductViewModel product);
    }
}
