﻿using Microsoft.AspNetCore.Mvc;

namespace KickSport.WebAPI.Models.Common
{
    public class BadRequestViewModel : ProblemDetails
    {
        public string Message { get; set; }
    }
}
