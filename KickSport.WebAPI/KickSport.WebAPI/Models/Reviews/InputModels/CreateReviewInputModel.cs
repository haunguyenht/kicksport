﻿using KickSport.Data.Common.Constant;
using System.ComponentModel.DataAnnotations;

namespace KickSport.WebAPI.Models.Reviews.InputModels
{
    public class CreateReviewInputModel
    {
        [Required]
        [MinLength(Validation.ReviewMinimumLength, ErrorMessage = Validation.ReviewErrorMessage)]
        public string Review { get; set; }
    }
}
