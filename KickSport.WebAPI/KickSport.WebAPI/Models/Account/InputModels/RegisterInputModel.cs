﻿using KickSport.Data.Common.Constant;
using System.ComponentModel.DataAnnotations;

namespace KickSport.WebAPI.Models.Account.InputModels
{
    public class RegisterInputModel
    {
        [Required]
        [EmailAddress(ErrorMessage = Validation.EmailErrorMessage)]
        public string Email { get; set; }

        [Required]
        [RegularExpression(Validation.UsernameRegex, ErrorMessage = Validation.UsernameRegexErrorMessage)]
        [MinLength(Validation.UsernameMinimumLength, ErrorMessage = Validation.UsernameErrorMessage)]
        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required]
        [MinLength(Validation.PasswordMinimumLength, ErrorMessage = Validation.PasswordErrorMessage)]
        public string Password { get; set; }
    }
}
