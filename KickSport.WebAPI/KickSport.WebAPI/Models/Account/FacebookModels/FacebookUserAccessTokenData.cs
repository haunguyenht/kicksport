﻿using Newtonsoft.Json;

namespace KickSport.WebAPI.Models.Account.FacebookModels
{
    public class FacebookUserAccessTokenData
    {
        [JsonProperty("is_valid")]
        public bool IsValid { get; set; }
    }
}
