﻿namespace KickSport.WebAPI.Models.Account.FacebookModels
{
    public class FacebookUserAccessTokenValidation
    {
        public FacebookUserAccessTokenData Data { get; set; }
    }
}
