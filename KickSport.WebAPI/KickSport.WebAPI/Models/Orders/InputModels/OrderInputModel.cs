﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KickSport.WebAPI.Models.Orders.InputModels
{
    public class OrderInputModel
    {
        [Required]
        public IEnumerable<OrderProductInputModel> OrderProducts { get; set; }
    }
}
