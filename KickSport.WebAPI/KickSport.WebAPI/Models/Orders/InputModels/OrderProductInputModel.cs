﻿using KickSport.Data.Common.Constant;
using System;
using System.ComponentModel.DataAnnotations;

namespace KickSport.WebAPI.Models.Orders.InputModels
{
    public class OrderProductInputModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Range(Validation.MinimumPrice, Double.MaxValue, ErrorMessage = Validation.PriceErrorMessage)]
        public decimal Price { get; set; }

        [Range(Validation.MinimumQuantity, int.MaxValue, ErrorMessage = Validation.QuantityErrorMessage)]
        public int Quantity { get; set; }
    }
}
