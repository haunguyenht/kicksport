﻿namespace KickSport.WebAPI.Models.Ingredients.ViewModels
{
    public class IngredientViewModel
    {
        public string Name { get; set; }
    }
}
