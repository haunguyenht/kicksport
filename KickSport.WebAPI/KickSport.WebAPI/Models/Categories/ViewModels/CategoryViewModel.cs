﻿namespace KickSport.WebAPI.Models.Categories.ViewModels
{
    public class CategoryViewModel
    {
        public string Name { get; set; }
    }
}
