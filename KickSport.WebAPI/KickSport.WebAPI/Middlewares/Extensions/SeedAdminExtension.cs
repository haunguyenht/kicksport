﻿using Microsoft.AspNetCore.Builder;

namespace KickSport.WebAPI.Middlewares.Extensions
{
    public static class SeedAdminExtension
    {
        public static IApplicationBuilder UseSeedAdminMiddleware(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SeedAdminMiddleware>();
        }
    }
}
