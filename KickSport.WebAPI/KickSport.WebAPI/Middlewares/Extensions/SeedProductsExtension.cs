﻿using Microsoft.AspNetCore.Builder;

namespace KickSport.WebAPI.Middlewares.Extensions
{
    public static class SeedProductsExtension
    {
        public static IApplicationBuilder UseSeedProductsMiddleware(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SeedProductsMiddleware>();
        }
    }
}
