﻿using Microsoft.AspNetCore.Builder;

namespace KickSport.WebAPI.Middlewares.Extensions
{
    public static class SeedIngredientsExtension
    {
        public static IApplicationBuilder UseSeedIngredientsMiddleware(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SeedIngredientsMiddleware>();
        }
    }
}
