﻿using Microsoft.AspNetCore.Builder;

namespace KickSport.WebAPI.Middlewares.Extensions
{
    public static class SeedCategoriesExtension
    {
        public static IApplicationBuilder UseSeedCategoriesMiddleware(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SeedCategoriesMiddleware>();
        }
    }
}
