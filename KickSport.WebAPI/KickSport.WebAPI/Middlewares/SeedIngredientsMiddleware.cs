﻿using KickSport.Services.DataServices.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KickSport.WebAPI.Middlewares
{
    public class SeedIngredientsMiddleware
    {
        private readonly RequestDelegate _next;

        public SeedIngredientsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, IServiceProvider provider)
        {
            var ingredientsService = provider.GetService<IIngredientsService>();
            if (!ingredientsService.Any())
            {
                await ingredientsService.CreateRangeAsync(new string[]
                {
                    "leather",
                    "plastic",
                    "wood",
                    "gold",
                    "platinum",
                    "ham",
                    "silver",
                    "cloth",
                    "cotton",
                    "leather",
                    "silk",
                    "rubber"
                });
            }

            await _next(context);
        }
    }
}
