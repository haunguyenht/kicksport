﻿using KickSport.Services.DataServices.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace KickSport.WebAPI.Middlewares
{
    public class SeedCategoriesMiddleware
    {
        private readonly RequestDelegate _next;

        public SeedCategoriesMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, IServiceProvider provider)
        {
            var categoriesService = provider.GetService<ICategoriesService>();
            if (!categoriesService.Any())
            {
                await categoriesService.CreateRangeAsync(new string[]
                {
                    "Nike",
                    "Adidas",
                    "Puma",
                    "New Balance",
                    "Air Jordan",
                    "ASICS",
                    "Converse",
                    "Vans",
                    "Under Armour"
                });
            }

            await _next(context);
        }
    }
}
