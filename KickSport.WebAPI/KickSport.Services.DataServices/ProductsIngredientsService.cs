﻿using KickSport.Data.Common;
using KickSport.Data.Models;
using KickSport.Services.DataServices.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KickSport.Services.DataServices
{
    public class ProductsIngredientsService : IProductsIngredientsService
    {
        private readonly IRepository<ProductsIngredients> _productsIngredientsRepository;

        public ProductsIngredientsService(IRepository<ProductsIngredients> productsIngredientsRepository)
        {
            _productsIngredientsRepository = productsIngredientsRepository;
        }

        public async Task DeleteProductIngredientsAsync(string productId)
        {
            var productIngredients = _productsIngredientsRepository
                .All()
                .Where(pi => pi.ProductId == productId)
                .ToList();

            if (productIngredients.Any())
            {
                _productsIngredientsRepository.DeleteRange(productIngredients);
                await _productsIngredientsRepository.SaveChangesAsync();
            }
        }
    }
}
