﻿using AutoMapper;
using KickSport.Data.Common;
using KickSport.Data.Models;
using KickSport.Services.DataServices.Contracts;
using KickSport.Services.DataServices.Models.Ingredients;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KickSport.Services.DataServices
{
    public class IngredientsService : IIngredientsService
    {
        private readonly IRepository<Ingredient> _ingredientsRepository;
        private readonly IMapper _mapper;

        public IngredientsService(
            IRepository<Ingredient> ingredientsRepository,
            IMapper mapper)
        {
            _ingredientsRepository = ingredientsRepository;
            _mapper = mapper;
        }

        public bool Any()
        {
            return _ingredientsRepository.All().Any();
        }

        public IEnumerable<IngredientDto> All()
        {
            return _ingredientsRepository
                .All()
                .Select(i => _mapper.Map<IngredientDto>(i))
                .OrderBy(i => i.Name);
        }

        public async Task CreateAsync(string ingredientName)
        {
            var ingredient = new Ingredient
            {
                Name = ingredientName
            };

            await _ingredientsRepository.AddAsync(ingredient);
            await _ingredientsRepository.SaveChangesAsync();
        }

        public async Task CreateRangeAsync(string[] ingredientsName)
        {
            var ingredients = ingredientsName.Select(ingredientName => new Ingredient
            {
                Name = ingredientName
            });

            await _ingredientsRepository.AddRangeAsync(ingredients);
            await _ingredientsRepository.SaveChangesAsync();
        }

        public IngredientDto FindByName(string ingredientName)
        {
            return _ingredientsRepository
                .All()
                .Select(i => _mapper.Map<IngredientDto>(i))
                .FirstOrDefault(i => i.Name.ToLower() == ingredientName.ToLower());
        }
    }
}
