﻿using AutoMapper;
using KickSport.Data.Common;
using KickSport.Data.Models;
using KickSport.Services.DataServices.Contracts;
using KickSport.Services.DataServices.Models.Products;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KickSport.Services.DataServices
{
    public class ProductsService : IProductsService
    {
        private readonly IRepository<Product> _productsRepository;
        private readonly IMapper _mapper;

        public ProductsService(
            IRepository<Product> productsRepository,
            IMapper mapper)
        {
            _productsRepository = productsRepository;
            _mapper = mapper;
        }

        public IEnumerable<ProductDto> All()
        {
            return _productsRepository
                .All()
                .AsNoTracking()
                .Include(p => p.Category)
                .Include(p => p.Ingredients)
                .ThenInclude(pi => pi.Ingredient)
                .Include(p => p.Likes)
                .ThenInclude(ul => ul.ApplicationUser)
                .Select(p => _mapper.Map<ProductDto>(p));
        }

        public bool Any()
        {
            return _productsRepository.All().Any();
        }

        public async Task CreateAsync(ProductDto productDto)
        {
            var product = _mapper
                .Map<Product>(productDto);

            await _productsRepository.AddAsync(product);
            await _productsRepository.SaveChangesAsync();
        }

        public async Task CreateRangeAsync(IEnumerable<ProductDto> productsDtos)
        {
            var products = productsDtos
                .Select(pdto => _mapper.Map<Product>(pdto));

            await _productsRepository.AddRangeAsync(products);
            await _productsRepository.SaveChangesAsync();
        }

        public async Task DeleteAsync(string productId)
        {
            var product = _productsRepository
                .All()
                .First(p => p.Id == productId);

            _productsRepository.Delete(product);
            await _productsRepository.SaveChangesAsync();
        }

        public async Task EditAsync(ProductDto productDto)
        {
            var product = _mapper
                .Map<Product>(productDto);

            _productsRepository.Update(product);
            await _productsRepository.SaveChangesAsync();
        }

        public bool Exists(string productId)
        {
            return _productsRepository
                .All()
                .Any(p => p.Id == productId);
        }
    }
}
