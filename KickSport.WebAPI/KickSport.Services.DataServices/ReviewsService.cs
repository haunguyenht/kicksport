﻿using AutoMapper;
using KickSport.Data.Common;
using KickSport.Data.Models;
using KickSport.Services.DataServices.Contracts;
using KickSport.Services.DataServices.Models.Reviews;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KickSport.Services.DataServices
{
    public class ReviewsService : IReviewsService
    {
        private readonly IRepository<Review> _reviewsRepository;
        private readonly IMapper _mapper;

        public ReviewsService(
            IRepository<Review> reviewsRepository,
            IMapper mapper)
        {
            _reviewsRepository = reviewsRepository;
            _mapper = mapper;
        }

        public async Task<ReviewDto> CreateAsync(string text, string creatorId, string productId)
        {
            var review = new Review
            {
                Text = text,
                CreatorId = creatorId,
                ProductId = productId,
                LastModified = DateTime.Now
            };

            await _reviewsRepository.AddAsync(review);
            await _reviewsRepository.SaveChangesAsync();

            return _mapper.Map<ReviewDto>(review);
        }

        public IEnumerable<ReviewDto> GetProductReviews(string productId)
        {
            return _reviewsRepository
                .All()
                .Include(r => r.Creator)
                .Where(r => r.ProductId == productId)
                .Select(r => _mapper.Map<ReviewDto>(r))
                .ToList();
        }

        public async Task DeleteProductReviewsAsync(string productId)
        {
            var reviews = _reviewsRepository
                .All()
                .Where(r => r.ProductId == productId)
                .ToList();

            if (reviews.Any())
            {
                _reviewsRepository.DeleteRange(reviews);
                await _reviewsRepository.SaveChangesAsync();
            }
        }

        public async Task DeleteReviewAsync(string reviewId)
        {
            var review = _reviewsRepository
                .All()
                .First(r => r.Id == reviewId);

            _reviewsRepository.Delete(review);
            await _reviewsRepository.SaveChangesAsync();
        }

        public bool Exists(string reviewId)
        {
            return _reviewsRepository
                .All()
                .Any(r => r.Id == reviewId);
        }

        public string FindReviewCreatorById(string reviewId)
        {
            return _reviewsRepository
                .All()
                .Include(r => r.Creator)
                .First(r => r.Id == reviewId)
                .Creator
                .UserName;
        }
    }
}
