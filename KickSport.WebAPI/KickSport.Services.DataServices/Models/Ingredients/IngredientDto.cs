﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KickSport.Services.DataServices.Models.Ingredients
{
    public class IngredientDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
