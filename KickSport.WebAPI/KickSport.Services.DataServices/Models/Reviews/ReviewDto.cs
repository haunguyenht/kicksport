﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KickSport.Services.DataServices.Models.Reviews
{
    public class ReviewDto
    {
        public string Id { get; set; }

        public string ReviewText { get; set; }

        public string CreatorUsername { get; set; }

        public DateTime LastModified { get; set; }
    }
}
