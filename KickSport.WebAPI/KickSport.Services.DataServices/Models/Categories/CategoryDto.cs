﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KickSport.Services.DataServices.Models.Categories
{
    public class CategoryDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
