﻿using System.Threading.Tasks;

namespace KickSport.Services.DataServices.Contracts
{
    public interface IProductsIngredientsService
    {
        Task DeleteProductIngredientsAsync(string productId);
    }
}
