﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using KickSport.Data.Common;
using KickSport.Data.Models;
using KickSport.Services.DataServices.Contracts;
using KickSport.Services.DataServices.Models.Categories;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;

namespace KickSport.Services.DataServices
{
    public class CategoriesService : ICategoriesService
    {
        private readonly IRepository<Category> _categoriesRepository;
        private readonly IMapper _mapper;

        public CategoriesService(
            IRepository<Category> categoriesRepository,
            IMapper mapper)
        {
            _categoriesRepository = categoriesRepository;
            _mapper = mapper;
        }

        public IEnumerable<CategoryDto> All()
        {
            return _categoriesRepository
                .All()
                .ProjectTo<CategoryDto>(_mapper.ConfigurationProvider)
                .OrderBy(c => c.Name).ToList();
        }

        public bool Any()
        {
            return _categoriesRepository.All().Any();
        }

        public async Task CreateAsync(string categoryName)
        {
            var category = new Category
            {
                Name = categoryName
            };

            await _categoriesRepository.AddAsync(category);
            await _categoriesRepository.SaveChangesAsync();
        }

        public async Task CreateRangeAsync(string[] categoriesName)
        {
            var categories = categoriesName
                .Select(categoryName => new Category
                {
                    Name = categoryName
                });

            await _categoriesRepository.AddRangeAsync(categories);
            await _categoriesRepository.SaveChangesAsync();
        }

        public CategoryDto FindByName(string categoryName)
        {
            var x = _categoriesRepository.FindOneAsync(x => x.Name == categoryName);
            var result = _mapper.Map<CategoryDto>(x);
            return result;
            //return _categoriesRepository
            //    .All()
            //    .Select(c => _mapper.Map<CategoryDto>(c))
            //    .FirstOrDefault(c => c.Name == categoryName);
        }

    }
}
