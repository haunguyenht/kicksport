﻿using KickSport.Data.Common;
using System.Collections.Generic;

namespace KickSport.Data.Models
{
    public class OrderProduct : BaseModel<int>
    {
        public string ProductId { get; set; }

        public Product Product { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }
    }
}
