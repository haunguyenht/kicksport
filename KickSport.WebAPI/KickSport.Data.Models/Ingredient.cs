﻿using System;
using System.Collections.Generic;
using System.Text;
using KickSport.Data.Common;

namespace KickSport.Data.Models
{
    public class Ingredient : BaseModel<int>
    {
        public string Name { get; set; }

        public ICollection<ProductsIngredients> Products { get; set; }
    }
}
