﻿using System;
using KickSport.Data.Common;
using System.Collections.Generic;
using KickSport.Data.Models.Enums;

namespace KickSport.Data.Models
{
    public class Order : BaseModel<string>
    {
        public string CreatorId { get; set; }

        public ApplicationUser Creator { get; set; }

        public OrderStatus Status { get; set; }

        public DateTime CreationDate { get; set; }

        public ICollection<OrderProduct> Products { get; set; }
    }
}
